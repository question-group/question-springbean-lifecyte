package com.snailmann.question.bean;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Slf4j
@Data
@Component
public class Hello implements InitializingBean, DisposableBean {


    @Autowired
    public String hhh;
    public String aaa = new String("aaa");
    public String ccc = "ccc";
    /**
     * 成员属性
     */
    public String helloworld = "helloworld";

    /**
     * 静态代码块，类加载时输出
     */
    static {
        log.warn("【static】 Hello class was loading{}");
    }


    /**
     * 无参构造函数
     */
    public Hello() {
        log.warn("【Constructor】 - initialization ");
    }

    /**
     * 自定义Bean初始化方法
     */
    @PostConstruct
    public void postInit() {
        log.warn("【PostConstruct】 - postInit");
    }

    /**
     * 自定义Bean注销方法
     */
    @PreDestroy
    public void postDestroy() {
        log.warn("【PreDestroy】 - postDestroy");
    }


    public void beanInit() {
        log.warn("【Bean】 init");
    }


    public void beanDestory() {
        log.warn("【Bean】 destory");
    }


    /**
     * 实现InitializingBean的方法，自定义初始化逻辑
     *
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        log.warn("【InitializingBean】 - afterPropertiesSet");
    }

    /**
     * 实现DisposableBean的方法，自定义销毁逻辑
     *
     * @throws Exception
     */
    @Override
    public void destroy() throws Exception {
        log.warn("【DisposableBean】 - destroy");
    }
}
