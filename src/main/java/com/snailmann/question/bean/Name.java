package com.snailmann.question.bean;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class Name {

    public String name = "jerry";

    public Name(){ }

    public Name(String name) {
        this.name = name;
    }
}
