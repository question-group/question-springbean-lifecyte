package com.snailmann.question;

import com.snailmann.question.bean.Hello;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@Slf4j
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(Application.class, args);
        Hello hello = applicationContext.getBean(Hello.class);
        log.warn("{}",((ConfigurableApplicationContext) applicationContext).getBeanFactory().getBeanDefinition("hello").getPropertyValues());
        log.warn(hello.toString());
        ((ConfigurableApplicationContext) applicationContext).close();
    }

}

