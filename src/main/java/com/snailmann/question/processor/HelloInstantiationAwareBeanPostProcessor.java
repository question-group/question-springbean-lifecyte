package com.snailmann.question.processor;

import com.snailmann.question.bean.Hello;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class HelloInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {


    /**
     * postProcessBeforeInstantiation方法的作用在目标对象被实例化之前调用的方法，可以返回目标实例的一个代理用来代替目标实例
     * 相比BeanFacotoryPostProcessor的方法，都是在实例化前调用，但是这里不能操作bean的元数据(BeanDefinition)
     * 返回值类型是Object，如果返回值的是非null对象，那么接下来只有postProcessAfterInitialization方法会被执行
     * 返回值如果是null,剩下的过程都会执行
     *
     * @param beanClass 目标对象的类型
     * @param beanName 目标实例在Spring容器中的name
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {

        if ("hello".equals(beanName)) {
            log.warn("【HelloInstantiationAwareBeanPostProcessor】 - before");
            return null;
        }

        return null;
    }

    /**
     * postProcessAfterInstantiation方法的作用在目标对象被实例化之后并且在属性值被populate之前调用
     *
     * @param bean   拦截的bean实体
     * @param beanName 目标bean的id
     * @return
     * @throws BeansException
     */
    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        if ("hello".equals(beanName)) {
            log.warn("【HelloInstantiationAwareBeanPostProcessor】 - after");
            return true;
        }

        return true;
    }

    /**
     * postProcessPropertyValues方法的作用在属性中注入到目标实例之前调用，可以修改属性的设置
     * 虽然是在注入属性前修改的属性，但最终的情况仍然是以本次修改为结果，即后面发生的注入行为不会覆盖本次修改的结果
     *
     * @param pvs bean元数据中的MutablePropertyValues
     * @param bean 当前实例化好的对象
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
        if ("hello".equals(beanName)) {

            //PropertyValues的修改才会呈现最后效果的
            ((MutablePropertyValues)pvs).addPropertyValue("helloworld","postProcessProperties - helloworld");
/*            ((MutablePropertyValues)pvs).addPropertyValue("name",new Name("hhhh"));*/

            //bean的修改是没有任何效果的
            ((Hello)bean).setHelloworld("helloworld");
            log.warn("【HelloInstantiationAwareBeanPostProcessor】 - postProcessProperties");

            return pvs;
        }



        return pvs;
    }
}
