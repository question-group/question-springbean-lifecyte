package com.snailmann.question.processor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;


/**
 * 后处理器
 */
@Component
@Slf4j
public class HelloBeanPostProcessor implements BeanPostProcessor, BeanFactoryAware {

    private BeanFactory beanFactory;

    /**
     * 在bean实例化、属性注入之后，自定义初始化操作之前进行，可以对已经实例化好的bean进行修改
     *
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            if ("hello".equals(beanName)){
                log.warn("【HelloBeanPostProcessor】 - before");
                return bean;
            }

            return bean;
    }


    /**
     * 在bean实例化、属性注入、自定义初始化操作之后进行，可以对自定义初始化后的bean进行修改
     *
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if ("hello".equals(beanName)){
            log.warn("【HelloBeanPostProcessor】 - after");
            return bean;
        }

        return bean;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}
