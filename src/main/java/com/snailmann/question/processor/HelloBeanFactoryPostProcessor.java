package com.snailmann.question.processor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * 工厂后处理器
 */
@Component
@Slf4j
public class HelloBeanFactoryPostProcessor implements BeanFactoryPostProcessor {


    /**
     * 可以对bean实例化前，通过Ioc容器获取到bean的元数据，并对其元数据进行修改
     * @param beanFactory
     * @throws BeansException
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        //通过ioc容器，获取com.example.demo3.bean.Hello类的元数据
        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("hello");

        //从元数据中获得该Bean的成员参数(成员属性...)
        MutablePropertyValues mutablePropertyValues = beanDefinition.getPropertyValues();

        //修改元数据中的属性
        /*mutablePropertyValues.add("helloworld","HelloBeanFactoryPostProcessor - helloworld");*/
        log.warn("【HelloBeanFactoryPostProcessor】 - postProcessBeanFactory");
    }
}
